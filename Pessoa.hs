module Pessoa ( Pessoa(..) ) where

data Pessoa = Pessoa { nome  :: [Char]
                     , idade :: Int
                     } deriving (Show)
