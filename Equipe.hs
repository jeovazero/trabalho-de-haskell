module Equipe ( Equipe(..), adicionarPessoa, removerPessoa, novaPessoa) where

import Pessoa ( Pessoa(..) )


data Equipe = Equipe { nomeEquipe    :: [Char]
                     , membros       :: [Pessoa]
                     } deriving ( Show )

novaPessoa :: [Char] -> Int -> Pessoa
novaPessoa n i = Pessoa { nome=n, idade=i }

adicionarPessoa :: Equipe -> Pessoa -> Equipe
adicionarPessoa equipe pessoa =
    equipe { membros = (membros equipe ++ [pessoa] ) }


auxRemoverPessoa :: [Pessoa] -> Int -> [Pessoa]
auxRemoverPessoa (x:xs) n
    | n == 0 = xs
    | otherwise = x:auxRemoverPessoa xs (n - 1)

removerPessoa :: Equipe -> Int -> Equipe
removerPessoa equipe num =
    equipe { membros = auxRemoverPessoa (membros equipe) num }
