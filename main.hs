import Equipe ( Equipe(..), novaPessoa, adicionarPessoa, removerPessoa )
import Pessoa
import System.Exit
import System.IO

lerArquivo :: [Char] -> IO [Char]
lerArquivo destino = do
    conteudo <- readFile destino
    putStr conteudo
    return conteudo


recuperarMembros :: [[Char]] -> Int -> ([Pessoa], [[Char]])
recuperarMembros linhas 0 = ([],linhas)
recuperarMembros (linha:outrasLinhas) qLinhas =
    (pessoa:listaPessoas, outrasLinhas')
        where   [nome', idadeStr] = words linha
                idade' = (read idadeStr) :: Int
                pessoa = novaPessoa nome' idade'
                (listaPessoas, outrasLinhas') = recuperarMembros outrasLinhas (qLinhas - 1)


auxRecuperarEquipe :: [[Char]] -> [Equipe]
auxRecuperarEquipe [] = []
auxRecuperarEquipe (linha:outrasLinhas) = equipe:(auxRecuperarEquipe linhas')
    where   [nomeEquipe', qmembros] = (words linha)
            qmembros' = (read qmembros) :: Int
            (membros', linhas') = recuperarMembros outrasLinhas qmembros'
            equipe = Equipe { nomeEquipe=nomeEquipe', membros= membros'}

recuperarEquipes :: [Char] -> IO [Equipe]
recuperarEquipes arquivo = do
    conteudo <- lerArquivo arquivo
    let listaLinhas = lines conteudo
    let ans = auxRecuperarEquipe listaLinhas
    return ans


serializarMembro :: Pessoa -> [Char]
serializarMembro membro =
    (nome membro) ++ " " ++ (show $ idade membro) ++ "\n"

serializarMembros :: [Pessoa] -> [Char]
serializarMembros [] = []
serializarMembros (m:ms) =
    (serializarMembro m) ++ (serializarMembros ms)

serializarEquipe :: Equipe -> [Char]
serializarEquipe equipe =
    (nomeEquipe equipe) ++ " " ++
    (show $ length (membros equipe)) ++ "\n" ++
    serializarMembros (membros equipe)

serializarEquipes :: [Equipe] -> [Char]
serializarEquipes [] = []
serializarEquipes (e:es) =
    (serializarEquipe e) ++ (serializarEquipes es)

salvarEquipes :: [Equipe] -> [Char] -> IO ()
salvarEquipes equipes arquivo = do
    let conteudo = serializarEquipes equipes
    print conteudo
    writeFile arquivo conteudo

-- ordenaListaEquipes :: [Equipe] -> [Equipe]

--------------------------------------------------
-- Gerenciando equipes
adicionarEquipe :: [Equipe] -> Equipe -> [Equipe]
adicionarEquipe lista equipe = equipe:lista

removerEquipe :: [Equipe] -> Int -> [Equipe]
removerEquipe (x:xs) num
  | num == 0  = xs
  | otherwise = x:(removerEquipe xs (num - 1))
-------------------------------------------------

-------------------------------------------------
-- Exibindo
mostrarMembros :: [Pessoa] -> Int -> IO ()
mostrarMembros [] 0 = putStrLn "\t--------"
mostrarMembros [] _ = return ()
mostrarMembros (p:ps) num = do
    putStrLn $ "\t ID: " ++ (show num)
    putStrLn $ "\t Nome : " ++ (nome p)
    putStrLn $ "\t Idade: " ++ (show $ idade p)
    putStrLn $ "\t - = - = - = - = - = - = - "
    mostrarMembros ps (num+1)

listarEquipes :: [Equipe] -> Int -> Bool -> IO ()
listarEquipes [] 0 _  = putStrLn " Não há equipes! "
listarEquipes [] _ _ = return ()
listarEquipes (x:xs) n comMembros= do
    putStrLn $ "--------------------------------------"
    putStrLn $ "|  ID: " ++ (show n)
    putStrLn $ "|  Nome da equipe: " ++ (nomeEquipe x)
    if comMembros == True then mostrarMembros (membros x) 0
                          else return ()
    listarEquipes xs (n+1) comMembros


listarEquipeMenu :: [Equipe] -> IO [Equipe]
listarEquipeMenu equipes = do
    listarEquipes equipes 0 True
    return equipes


adicionarEquipeMenu :: [Equipe] -> IO [Equipe]
adicionarEquipeMenu equipes = do
    putStrLn $ "====== ADICIONAR EQUIPE ======="
    putStr $ "| Nome da equipe> "
    _nome <- getLine
    let novaEquipe = Equipe {nomeEquipe=_nome, membros=[]}
    let ans = adicionarEquipe equipes novaEquipe
    salvarEquipes ans "equipes.txt"
    return ans


removerEquipeMenu :: [Equipe] -> IO [Equipe]
removerEquipeMenu equipes = do
    putStrLn $ "======== REMOVER EQUIPE ========="
    putStrLn "| Escolha abaixo o numero da equipe para remover:"
    listarEquipes equipes 0 False
    putStr "| opcao> "
    op <- lerInt
    let ans = removerEquipe equipes op
    salvarEquipes ans "equipes.txt"
    return ans

adicionarMembroMenu :: Equipe -> IO Equipe
adicionarMembroMenu equipe = do
    putStrLn "| Nome do membro: "
    _nome <- getLine
    putStrLn "| Idade do membro: "
    _idade <- lerInt
    let pessoa = novaPessoa _nome _idade
    return (adicionarPessoa equipe pessoa)


removerMembroMenu :: Equipe -> IO Equipe
removerMembroMenu equipe = do
    putStrLn "| Escolha abaixo o numero do membro para remover:"
    mostrarMembros (membros equipe) 0
    putStr "| numero> "
    num <- lerInt
    let ans = (removerPessoa equipe num)
    return ans


gerenciarEquipeMenu :: Equipe -> IO Equipe
gerenciarEquipeMenu equipe = do
    putStrLn $ "==== GERENCIAR EQUIPE :: " ++ (nomeEquipe equipe) ++  " ===="
    putStrLn "| 1 - Adicionar membro"
    putStrLn "| 2 - Remover membro"
    putStrLn "| 0 - Voltar"
    putStr " | opcao> "
    op <- lerInt
    resultado <- case op of 1 -> adicionarMembroMenu equipe
                            2 -> removerMembroMenu equipe
                            0 -> do return equipe
                            _ -> error "Opcao errada"
    return resultado


selecionarEquipe :: [Equipe] -> Int -> IO [Equipe]
selecionarEquipe (x:xs) op
  | op == 0 = do
        x' <- (gerenciarEquipeMenu x)
        let ans = x':xs
        return ans
  | otherwise = do
        xs' <- selecionarEquipe xs (op-1)
        let ans = x:xs'
        return ans


selecionarEquipeMenu :: [Equipe] -> IO [Equipe]
selecionarEquipeMenu equipes = do
    putStrLn $ "======== SELECIONAR EQUIPE ========="
    putStrLn "| Escolha abaixo o numero da equipe"
    listarEquipes equipes 0 False
    putStr "| opcao> "
    op <- lerInt
    ans <- (selecionarEquipe equipes op)
    salvarEquipes ans "equipes.txt"
    return ans



operacaoEquipes :: Int -> [Equipe] -> IO [Equipe]
operacaoEquipes n equipes
  | n == 1 = listarEquipeMenu       equipes
  | n == 2 = adicionarEquipeMenu    equipes
  | n == 3 = removerEquipeMenu      equipes
  | n == 4 = selecionarEquipeMenu   equipes
  | n == 0 = exitSuccess
  | otherwise = exitFailure


lerInt :: IO Int
lerInt = do
    op <- getLine
    let opInt = (read op) :: Int
    return opInt

menu :: [Equipe] -> IO()
menu equipes = do
    putStrLn "======== MENU =================="
    putStrLn "1 - Listar    Equipes"
    putStrLn "2 - Adicionar Equipe"
    putStrLn "3 - Remover   Equipe"
    putStrLn "4 - Selecionar Equipe"
    putStrLn "0 - Sair\n"
    putStr "| opcao:> "

    opcao <- lerInt
    resultado <- operacaoEquipes opcao equipes
    menu resultado


main = do
    equipes <- recuperarEquipes "equipes.txt"
    {- 
    let p1 = novaPessoa "jeova" 20
    let p2 = novaPessoa "kingrd" 21
    let p3 = novaPessoa "duster" 22

    let e = Equipe { nomeEquipe="liquid", membros=[] }
    print $ e

    let e1 = adicionarPessoa (adicionarPessoa (adicionarPessoa e p1) p2) p3
    print e1

    print $ removerPessoa e1 0
    print $ removerPessoa e1 1
    print $ removerPessoa e1 2
    -}
    menu equipes
